---
title: "I installed Ubuntu after 8 years"
date: 2020-04-27T12:46:12-05:00
draft: falseg
---

Some days ago I installed Ubuntu after 8 years without using it, the version I installed is **Ubuntu 20.04 LTS**,
honestly I don't remember how it looked like the last time I use it, what I remember is that I stop using it because of
the change to the Unity Desktop. But recently I was feeling a bit nostalgic about it and I decided to give it a try
again to see how it evolved, yes I read somewhere that Canonical dropped the Unity desktop project and switched back to
use vanilla GNOME, with some customization to make it look like it is Unity Desktop, honestly it doesn't look as bad as
I thought, since for the last 8 years I was using KDE Plasma as my main D.E.

So here it's a screenshot of how it looks like:

![Ubuntu 20.04 screenshot](../images/ubuntu-s001.png)


## First things I did after installing it.

1. One of the firsts things I did was installing `zsh` and customize it using [Oh-my-ZSH](https://ohmyz.sh/) and
    [PowerLevel10k](https://github.com/romkatv/powerlevel10k/), so this is how it looks like:

    ![zsh prompt](../images/ubuntu-s002.png)

1. I installed [pyenv](https://github.com/pyenv/pyenv) and [pyenv-virtualenv](https://github.com/pyenv/pyenv-virtualenv)
    so I can manage Python versions and environments when I need them, I don't know if I'm going to use Ubuntu to work,
    but I'll have it ready just in case.

1. And of course I installed [Telegram](https://desktop.telegram.org/).


By the time that I'm writing this post, I have Ubuntu installed for some hours only, maybe I'll update this post later
if I do some other interesting things with it, or maybe I'll post a new entry to talk about it.
